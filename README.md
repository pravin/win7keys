Win7Keys - Enable Windows 7 shortcuts on XP and Vista
=====================================================

This application enables Windows 7 shortcuts on Windows XP and Windows Vista. It's a tiny no-nonsense app that just works.
        
Keys Implemented
----------------

 + *Win + Left/Right* - Dock active application to the left/right side
 + *Win + Up* - Maximize active application
 + *Win + Down*  - Minimize active application


Source Code
-----------
This project has been written in C using the [Code::Blocks](http://www.codeblocks.org/) IDE.


Author
-------
You can contact the author [Pravin Paratey](http://pravin.insanitybegins.com) on the official 
[Win7Keys website](http://pravin.insanitybegins.com/dev/win7keys-windows-7-keys-emulator-for-xp-and-vista).